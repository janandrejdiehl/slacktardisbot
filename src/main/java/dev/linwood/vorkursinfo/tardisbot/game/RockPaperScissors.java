package dev.linwood.vorkursinfo.tardisbot.game;

public class RockPaperScissors {
    public enum Hand {
        ROCK, PAPER, SCISSOR, NONE
    }
    private final int gameId;
    private final String firstPlayer;
    private final String secondPlayer;
    private final String startDateTime;
    private String endDateTime;
    private Hand firstHand, secondHand;

    public RockPaperScissors(int gameId, String firstPlayer, String secondPlayer, String startDateTime, String endDateTime, String firstHand, String secondHand) {
        this.gameId = gameId;
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime == null ? "" : endDateTime;
        this.firstHand = firstHand == null || firstHand.isEmpty() ? Hand.NONE : Hand.valueOf(firstHand.toUpperCase());
        this.secondHand = secondHand == null || secondHand.isEmpty() ? Hand.NONE : Hand.valueOf(secondHand.toUpperCase());
    }

    public int getGameId() {
        return gameId;
    }

    public Hand getFirstHand() {
        return firstHand;
    }

    public Hand getSecondHand() {
        return secondHand;
    }

    public String getFirstPlayer() {
        return firstPlayer;
    }

    public String getSecondPlayer() {
        return secondPlayer;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime == null ? "" : endDateTime;
    }

    public void setFirstHand(Hand firstHand) {
        this.firstHand = firstHand;
    }

    public void setSecondHand(Hand secondHand) {
        this.secondHand = secondHand;
    }

    public String getWinningPlayer() {
        return switch (firstHand) {
            case ROCK -> switch (secondHand) {
                default -> "";
                case PAPER -> secondPlayer;
                case SCISSOR -> firstPlayer;
            };
            case PAPER -> switch (secondHand) {
                case ROCK -> firstPlayer;
                default -> "";
                case SCISSOR -> secondPlayer;
            };
            case SCISSOR -> switch (secondHand) {
                case ROCK -> secondPlayer;
                case PAPER -> firstPlayer;
                default -> "";
            };
            default -> "";
        };
    }
}
