package dev.linwood.vorkursinfo.tardisbot.game;

import com.slack.api.bolt.context.builtin.SlashCommandContext;
import com.slack.api.bolt.request.builtin.SlashCommandRequest;
import com.slack.api.bolt.response.Response;
import com.slack.api.methods.SlackApiException;
import dev.linwood.vorkursinfo.tardisbot.BotUtils;

import java.io.IOException;
import java.sql.SQLException;

public class GameCommand {
    private final GameManager gameManager;

    public GameCommand(GameManager gameManager) {
        this.gameManager = gameManager;
        register();
    }

    private void register() {
        gameManager.getApp().command("/rockpaperscissors", (r, c) -> {
            try {
                return onRockPaperScissors(r, c);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        });
    }

    private Response onRockPaperScissors(SlashCommandRequest request, SlashCommandContext context) throws SQLException, SlackApiException, IOException {
        var args = request.getPayload().getText().trim().split(" ");
        if (args.length != 2) {
            return context.ack("Wrong usage. Please use /rockpaperscissors <User> <rock/paper/scissor>");
        }
        var manager = gameManager.getRockPaperScissors();
        var locationManager = gameManager.getMain().getLocationManager();
        var current = request.getPayload().getUserId();
        var user = args[0];
        if (!user.startsWith("<@") || !user.endsWith(">")) {
            return context.ack("Wrong user");
        }
        user = user.substring(2, user.length() - 1);
        var slashIndex = user.indexOf("|");
        if (slashIndex >= 0) {
            user = user.substring(0, slashIndex);
        }
        RockPaperScissors.Hand hand;
        try {
            hand = RockPaperScissors.Hand.valueOf(args[1].toUpperCase());
        } catch (IllegalArgumentException exception) {
            return context.ack("Wrong hand, please use rock, paper or scissor");
        }

        var location = locationManager.getUserLocation(current);

        if (location == null) {
            return context.ack("You are currently not in the tardis");
        }

        var otherLocation = locationManager.getUserLocation(user);

        if (otherLocation == null) {
            return context.ack("The other player is currently not in the tardis");
        }

        if (!location.getPlace().equals(otherLocation.getPlace())) {
            return context.ack("You are not in the same place as the other player");
        }

        var game = manager.getGame(current, user, location);
        var end = BotUtils.dateTimeToString(location.getCurrentDateTime().isBefore(otherLocation.getCurrentDateTime()) ? otherLocation.getCurrentDateTime() : location.getCurrentDateTime());
        if (game != null) {
            if (game.getSecondHand() != null) {
                boolean updated = false;
                if (game.getSecondPlayer().equals(current)) {
                    if (game.getSecondHand() != RockPaperScissors.Hand.NONE) updated = true;
                    game.setSecondHand(hand);
                } else {
                    if (game.getFirstHand() != RockPaperScissors.Hand.NONE) updated = true;
                    game.setFirstHand(hand);
                }
                if (!updated) game.setEndDateTime(end);
                manager.saveGame(game);
                manager.sendUpdateMessage(game);
                if (updated)
                    return context.ack("Game updated. <@" + game.getWinningPlayer() + "> wins!");
                else
                    return context.ack("Turn accepted. <@" + game.getWinningPlayer() + "> wins!");
            }
            game.setFirstHand(hand);
            game.setEndDateTime(end);
            manager.saveGame(game);
            return context.ack("Game updated. You need to wait for <@" + user + "> to make a turn!");
        }
        manager.createGame(location, otherLocation, hand);
        manager.sendInviteMessage(user);
        return context.ack("Game created. You need to wait for a turn of the other player");

    }
}
