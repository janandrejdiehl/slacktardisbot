package dev.linwood.vorkursinfo.tardisbot.game;

import com.slack.api.bolt.App;
import dev.linwood.vorkursinfo.tardisbot.DatabaseManager;
import dev.linwood.vorkursinfo.tardisbot.Main;

public class GameManager {
    private final Main main;
    private final RockPaperScissorsManager rockPaperScissors;
    private final GameCommand gameCommand;

    public GameManager(Main main) {
        this.main = main;
        rockPaperScissors = new RockPaperScissorsManager(this);
        gameCommand = new GameCommand(this);
    }

    public GameCommand getGameCommand() {
        return gameCommand;
    }

    public Main getMain() {
        return main;
    }

    public DatabaseManager getDatabaseManager() {
        return main.getDatabaseManager();
    }

    public App getApp() {
        return main.getApp();
    }

    public RockPaperScissorsManager getRockPaperScissors() {
        return rockPaperScissors;
    }
}
