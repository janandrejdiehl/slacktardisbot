package dev.linwood.vorkursinfo.tardisbot.game;

import com.slack.api.methods.SlackApiException;
import com.slack.api.methods.request.chat.ChatPostMessageRequest;
import dev.linwood.vorkursinfo.tardisbot.BotUtils;
import dev.linwood.vorkursinfo.tardisbot.location.Location;
import dev.linwood.vorkursinfo.tardisbot.message.Message;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.sql.SQLException;

public class RockPaperScissorsManager {
    private final GameManager gameManager;
    private static final String CREATE_TABLE_STATEMENT = """
            CREATE TABLE IF NOT EXISTS rock_paper_scissors (
                                game_id SERIAL PRIMARY KEY,
                                first_player VARCHAR ( 50 ) NOT NULL DEFAULT '',
                                second_player VARCHAR ( 50 ) NOT NULL DEFAULT '',
                                first_hand VARCHAR ( 10 ) NOT NULL DEFAULT 'NONE',
                                second_hand VARCHAR ( 10 ) NOT NULL DEFAULT 'NONE',
                                place VARCHAR ( 50 ) NOT NULL DEFAULT '',
                                winning_player VARCHAR ( 50 ),
                                start_time TIMESTAMP NOT NULL,
                                end_time TIMESTAMP
            )
            """;
    private static final String UPDATE_END_GAME_STATEMENT =
            "UPDATE rock_paper_scissors SET first_hand = ?, second_hand = ?, start_time = TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI'), winning_player = ?, end_time = TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI') WHERE game_id = ?";
    private static final String UPDATE_NO_END_GAME_STATEMENT =
            "UPDATE rock_paper_scissors SET first_hand = ?, second_hand = ?, start_time = TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI'), winning_player = ? WHERE game_id = ?";
    private static final String INSERT_GAME_STATEMENT =
            "INSERT INTO rock_paper_scissors(first_player, second_player, place, first_hand, start_time) VALUES (?, ?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI'))";
    private static final String SELECT_GAME_STATEMENT =
            """
                            SELECT * FROM rock_paper_scissors WHERE ((first_player = ? AND second_player = ?)
                            OR (first_player = ? AND second_player = ?)) AND PLACE = ? AND
                            ((end_time IS NULL AND start_time <= TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI')) OR
                            (end_time IS NOT NULL AND TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI') BETWEEN start_time and end_time))
                            ORDER BY end_time ASC NULLS LAST
                    """;

    public RockPaperScissorsManager(GameManager gameManager) {
        this.gameManager = gameManager;
        createTable();
    }

    public @Nullable RockPaperScissors getGame(String firstPlayer, String secondPlayer, Location location) throws SQLException {
        var statement = gameManager.getDatabaseManager().preparedStatement(SELECT_GAME_STATEMENT);
        var place = location.getPlace();
        var time = BotUtils.dateTimeToString(location.getCurrentDateTime());
        statement.setString(1, firstPlayer);
        statement.setString(2, secondPlayer);
        statement.setString(3, secondPlayer);
        statement.setString(4, firstPlayer);
        statement.setString(5, place);
        statement.setString(6, time);
        statement.setString(7, time);
        var result = statement.executeQuery();
        if (!result.next()) return null;
        return new RockPaperScissors(result.getInt("game_id"),
                result.getString("first_player"),
                result.getString("second_player"),
                result.getString("start_time"),
                result.getString("end_time"),
                result.getString("first_hand"),
                result.getString("second_hand"));
    }

    public void createGame(Location firstPlayer, Location secondPlayer, RockPaperScissors.Hand hand) throws SQLException {
        var statement = gameManager.getDatabaseManager().preparedStatement(INSERT_GAME_STATEMENT);
        statement.setString(1, firstPlayer.getUserId());
        statement.setString(2, secondPlayer.getUserId());
        statement.setString(3, firstPlayer.getPlace());
        statement.setString(4, hand.name());
        var firstDateTime = firstPlayer.getCurrentDateTime();
        var secondDateTime = secondPlayer.getCurrentDateTime();
        var start_time = firstDateTime.isBefore(secondDateTime) ? firstDateTime : secondDateTime;
        statement.setString(5, BotUtils.dateTimeToString(start_time));
        statement.executeUpdate();
    }

    public void saveGame(RockPaperScissors game) throws SQLException {
        var statement = gameManager.getDatabaseManager().preparedStatement(game.getEndDateTime().isEmpty() ? UPDATE_NO_END_GAME_STATEMENT : UPDATE_END_GAME_STATEMENT);
        statement.setString(1, game.getFirstHand().name());
        statement.setString(2, game.getSecondHand().name());
        statement.setString(3, game.getStartDateTime());
        statement.setString(4, game.getWinningPlayer());
        if (!game.getEndDateTime().isEmpty()) {
            statement.setString(5, game.getEndDateTime());
            statement.setInt(6, game.getGameId());
        } else
            statement.setInt(5, game.getGameId());
        statement.executeUpdate();
    }

    public void sendUpdateMessage(RockPaperScissors game) throws SlackApiException, IOException {
        var winning = game.getWinningPlayer();
        for (var s : new String[]{game.getFirstPlayer(), game.getSecondPlayer()}) {
            gameManager.getApp().client().chatPostMessage(ChatPostMessageRequest.builder()
                    .channel(s)
                    .text("The game #" + game.getGameId() + " has been updated. <@" + winning + "> has won! " +
                            "The game has started at " + game.getStartDateTime() + " and ended at " + game.getEndDateTime() + ". " +
                            game.getFirstHand().name() + " (<@" + game.getFirstPlayer()
                            + ">) x " + game.getSecondHand().name() + "(<@" + game.getSecondPlayer() + ">)")
                    .build());
        }
    }

    public void sendInviteMessage(String userId) throws SlackApiException, IOException {
        gameManager.getApp().client().chatPostMessage(ChatPostMessageRequest.builder()
                .channel(userId)
                .text("<@" + userId + "> wants to play with you. Use /rockpaperscissor <@" + userId + "> <rock/paper/scissor> to make your turn.")
                .build());
    }

    private void createTable() {
        var statement = gameManager.getDatabaseManager().preparedStatement(CREATE_TABLE_STATEMENT);
        try {
            statement.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
