package dev.linwood.vorkursinfo.tardisbot;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class BotUtils {
    private BotUtils() {}
    public static String dateTimeToString(LocalDateTime dateTime) {
        return dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
    }
}
