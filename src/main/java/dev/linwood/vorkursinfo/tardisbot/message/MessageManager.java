package dev.linwood.vorkursinfo.tardisbot.message;

import com.slack.api.methods.SlackApiException;
import com.slack.api.methods.request.chat.ChatPostMessageRequest;
import dev.linwood.vorkursinfo.tardisbot.Main;
import dev.linwood.vorkursinfo.tardisbot.location.Location;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MessageManager {
    private final Main main;
    private static final String CREATE_TABLE_STATEMENT = """
            CREATE TABLE IF NOT EXISTS messages (
                                message_id SERIAL PRIMARY KEY,
                                place VARCHAR ( 50 ) NOT NULL,
                                user_id VARCHAR ( 255 ) NOT NULL,
                                message TEXT NOT NULL DEFAULT '',
                                sent TIMESTAMP NOT NULL
                            )
            """;
    private static final String UPDATE_MESSAGE_STATEMENT =
            """
    INSERT INTO messages(user_id, message, place, sent) VALUES (?, ?, ?, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI')) ON CONFLICT(message_id) DO
        UPDATE SET user_id = ?, message = ?, place = ?, sent = TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI')
    """;
    private static final String SELECT_MESSAGE_STATEMENT =
            "SELECT * FROM locations WHERE sent = TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI')";
    private static final String FIND_CURRENT_MESSAGES = """
            SELECT *, locations.user_id AS location_user, messages.user_id AS message_user FROM locations
             JOIN messages ON age(sent, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI')) = concat(delay,' minutes')::interval
            WHERE locations.place = messages.place
            """;
    private Timer checkNewMessageTimer = null;

    public MessageManager(Main main) {
        this.main = main;
        createTable();
        startCheckNewMessage();
    }

    public void startCheckNewMessage() {
        checkNewMessageTimer = new Timer();
        checkNewMessageTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    checkNewMessage();
                } catch (SQLException | SlackApiException | IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }, 0, 60 * 1000);
    }

    public boolean isCheckNewMessageRunning() {
        return checkNewMessageTimer != null;
    }

    public void stopCheckNewMessage() {
        checkNewMessageTimer.cancel();
    }


    public void checkNewMessage() throws SQLException, SlackApiException, IOException {
        var now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        var statement = main.getDatabaseManager().preparedStatement(FIND_CURRENT_MESSAGES);
        statement.setString(1, now);
        var result = statement.executeQuery();
        while(result.next()) {
            var userId = result.getString("location_user");
            var message = new Message(result.getString("message_user"),
                    result.getString("message"),
                    result.getString("place"),
                    result.getString("sent"));
            sendMessage(userId, message);
        }
    }

    public List<Message> getMessages(LocalDateTime dateTime) throws SQLException {
        var now = dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        var statement = main.getDatabaseManager().preparedStatement(SELECT_MESSAGE_STATEMENT);
        statement.setString(1, now);
        var result = statement.executeQuery();
        var messages = new ArrayList<Message>();
        while(result.next()) {
            messages.add(new Message(result.getString("user_id"),
                    result.getString("message"),
                    result.getString("place"),
                    result.getString("sent")));
        }
        return messages;
    }

    public List<Message> getMessages(int delay) throws SQLException {
        return getMessages(LocalDateTime.now().plusMinutes(delay));
    }

    public void saveMessage(Message message) throws SQLException {
        var statement = main.getDatabaseManager().preparedStatement(UPDATE_MESSAGE_STATEMENT);
        statement.setString(1, message.getUserId());
        statement.setString(2, message.getMessage());
        statement.setString(3, message.getPlace());
        statement.setString(4, message.getSent());
        statement.setString(5, message.getUserId());
        statement.setString(6, message.getMessage());
        statement.setString(7, message.getPlace());
        statement.setString(8, message.getSent());
        statement.executeUpdate();
    }

    public void saveAndSendMessage(Location current, String message) throws SQLException {
        var dateTime = current.getCurrentDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        var messageObject = new Message(current.getUserId(), message, current.getPlace(), dateTime);
        saveMessage(messageObject);
        main.getLocationManager().find(current).forEach(s -> {
            try {
                sendMessage(s, messageObject);
            } catch (SlackApiException | IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public void sendMessage(String userId, Message message) throws SlackApiException, IOException {
        main.getApp().client().chatPostMessage(ChatPostMessageRequest.builder()
                .channel(userId)
                .text("A message by <@" + message.getUserId() + ">: " + message.getMessage())
                .build());
    }

    private void createTable() {
        var statement = main.getDatabaseManager().preparedStatement(CREATE_TABLE_STATEMENT);
        try {
            statement.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
