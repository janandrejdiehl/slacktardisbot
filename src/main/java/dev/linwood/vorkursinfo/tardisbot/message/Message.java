package dev.linwood.vorkursinfo.tardisbot.message;

public class Message {
    private final String userId;
    private String message;
    private String sent;
    private String place;

    public Message(String userId, String message, String place, String sent) {
        this.userId = userId;
        this.message = message;
        this.place = place;
        this.sent = sent;
    }

    public String getUserId() {
        return userId;
    }

    public String getMessage() {
        return message;
    }

    public String getPlace() {
        return place;
    }

    public String getSent() {
        return sent;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public void setSent(String sent) {
        this.sent = sent;
    }
}
