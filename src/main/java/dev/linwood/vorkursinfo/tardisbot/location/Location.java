package dev.linwood.vorkursinfo.tardisbot.location;

import java.time.LocalDateTime;

public class Location {
    private final String userId;
    private String place;
    private int currentDelay;

    public Location(String userId, String place, int currentDelay) {
        this.userId = userId;
        this.place = place;
        this.currentDelay = currentDelay;
    }
    public Location(String userId) {
        this(userId, "", 0);
    }

    public String getUserId() {
        return userId;
    }

    public String getPlace() {
        return place;
    }

    public int getCurrentDelay() {
        return currentDelay;
    }

    public LocalDateTime getCurrentDateTime() {
        return LocalDateTime.now().plusMinutes(currentDelay);
    }

    public void setCurrentDelay(int currentDelay) {
        this.currentDelay = currentDelay;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public boolean isAtHome() {
        return place.equals("") && currentDelay == 0;
    }
}
