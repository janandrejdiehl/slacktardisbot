package dev.linwood.vorkursinfo.tardisbot.location;

import dev.linwood.vorkursinfo.tardisbot.DatabaseManager;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LocationManager {
    private static final String CREATE_TABLE_STATEMENT =
            """
                            CREATE TABLE IF NOT EXISTS locations (
                                user_id VARCHAR ( 255 ) PRIMARY KEY,
                                place VARCHAR ( 50 ) NOT NULL DEFAULT '',
                                delay integer NOT NULL DEFAULT 0
                            )
                    """;
    private static final String SELECT_LOCATION_STATEMENT =
            "SELECT * FROM locations WHERE user_id = ?";
    private static final String UPDATE_LOCATION_STATEMENT =
            """
    INSERT INTO locations(place, delay, user_id) VALUES (?, ?, ?) ON CONFLICT(user_id) DO
        UPDATE SET place = ?, delay = ?
    """;
    private static final String DELETE_LOCATION_STATEMENT =
            "DELETE FROM locations WHERE user_id = ?";
    private static final String FIND_USERS_STATEMENT =
            "SELECT user_id FROM locations WHERE place = ? AND delay = ?";
    private final DatabaseManager databaseManager;

    public LocationManager(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
        createTable();
    }

    public @Nullable Location getUserLocation(String user) throws SQLException {
        var statement = databaseManager.preparedStatement(SELECT_LOCATION_STATEMENT);
        statement.setString(1, user);
        var result = statement.executeQuery();
        if (!result.next()) return null;
        return new Location(
                result.getString("user_id"),
                result.getString("place"),
                result.getInt("delay"));
    }

    public boolean removeUserLocation(String user) throws SQLException {
        var statement = databaseManager.preparedStatement(DELETE_LOCATION_STATEMENT);
        statement.setString(1, user);
        return statement.executeUpdate() != 0;
    }

    public List<String> find(Location location) throws SQLException {
        var statement = databaseManager.preparedStatement(FIND_USERS_STATEMENT);
        statement.setString(1, location.getPlace());
        statement.setInt(2, location.getCurrentDelay());
        var result = statement.executeQuery();
        var users = new ArrayList<String>();
        while(result.next()) {
            users.add(result.getString("user_id"));
        }
        return users;
    }

    private void createTable() {
        var statement = databaseManager.preparedStatement(CREATE_TABLE_STATEMENT);
        try {
            statement.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void saveLocation(Location location) throws SQLException {
        var statement = databaseManager.preparedStatement(UPDATE_LOCATION_STATEMENT);
        statement.setString(1, location.getPlace());
        statement.setInt(2, location.getCurrentDelay());
        statement.setString(3, location.getUserId());
        statement.setString(4, location.getPlace());
        statement.setInt(5, location.getCurrentDelay());
        statement.executeUpdate();
    }
}
