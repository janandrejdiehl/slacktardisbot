package dev.linwood.vorkursinfo.tardisbot;

import com.slack.api.app_backend.events.payload.EventsApiPayload;
import com.slack.api.bolt.App;
import com.slack.api.bolt.AppConfig;
import com.slack.api.bolt.context.builtin.EventContext;
import com.slack.api.bolt.response.Response;
import com.slack.api.bolt.socket_mode.SocketModeApp;
import com.slack.api.methods.SlackApiException;
import com.slack.api.model.event.AppMentionEvent;
import dev.linwood.vorkursinfo.tardisbot.game.GameManager;
import dev.linwood.vorkursinfo.tardisbot.location.LocationManager;
import dev.linwood.vorkursinfo.tardisbot.message.MessageManager;

import java.io.IOException;

public class Main {
    private final CommandManager commandManager;
    private final LocationManager locationManager;
    private final DatabaseManager databaseManager;
    private final MessageManager messageManager;
    private final GameManager gameManager;
    private final App app;

    public static void main(String[] args) throws Exception {
        var config = new Configuration();
        new Main(config);
    }

    public Main(Configuration config) throws Exception {
        app = new App(AppConfig.builder().singleTeamBotToken(config.getBotToken()).build());
        SocketModeApp socketModeApp = new SocketModeApp(config.getAppToken(), app);

        app.event(AppMentionEvent.class, Main::mentioned);
        commandManager = new CommandManager(this);
        databaseManager = new DatabaseManager(config.getDatabaseUrl());
        databaseManager.connect();
        locationManager = new LocationManager(databaseManager);
        messageManager = new MessageManager(this);
        gameManager = new GameManager(this);
        socketModeApp.start();
    }

    public GameManager getGameManager() {
        return gameManager;
    }

    public CommandManager getCommandManager() {
        return commandManager;
    }

    public App getApp() {
        return app;
    }

    public LocationManager getLocationManager() {
        return locationManager;
    }

    private static Response mentioned(EventsApiPayload<AppMentionEvent> payload, EventContext eventContext) throws SlackApiException, IOException {
        if (payload.getEvent().getBotId() != null) return eventContext.ack();
        eventContext.say("""
                /whereami
                Returns the current place and time.
                                
                /travel <Place>
                Travel to a place.
                                
                /timetravel <Minutes>
                Travel through time. If the number is negative you travel in the past.
                                
                /meet
                Find people in the same time and place.
                                
                /tell <Message...>
                Use this command to say something.
                                
                /entertardis
                Use this to enter the tardis without travel through time or space.
                                
                /exittardis
                Use this to exit the tardis and don't get new notifications. Resets your time and space.
                                
                /rockpaperscissors <User> <rock/paper/scissor>
                Use this to play rock, paper, scissors with someone.
                
                Code can be found on GitLab: https://git.rwth-aachen.de/janandrejdiehl/slacktardisbot
                """);
        return eventContext.ack();
    }

    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    public MessageManager getMessageManager() {
        return messageManager;
    }
}
