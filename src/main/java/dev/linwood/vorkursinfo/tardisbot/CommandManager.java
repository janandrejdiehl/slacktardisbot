package dev.linwood.vorkursinfo.tardisbot;

import com.slack.api.bolt.context.builtin.SlashCommandContext;
import com.slack.api.bolt.request.builtin.SlashCommandRequest;
import com.slack.api.bolt.response.Response;
import dev.linwood.vorkursinfo.tardisbot.location.Location;

import java.sql.SQLException;
import java.time.format.DateTimeFormatter;

public class CommandManager {
    private final Main main;
    public CommandManager(Main main) {
        this.main = main;
        register();
    }

    private void register() {
        main.getApp().command("/whereami", this::onWhereAmI);
        main.getApp().command("/timetravel", this::onTimeTravel);
        main.getApp().command("/travel", this::onTravel);
        main.getApp().command("/meet", this::onMeet);
        main.getApp().command("/tell", this::onTell);
        main.getApp().command("/entertardis", this::onEnterTardis);
        main.getApp().command("/exittardis", this::onExitTardis);
    }

    private Response onEnterTardis(SlashCommandRequest request, SlashCommandContext context) {
        try {
            main.getLocationManager().saveLocation(new Location(request.getPayload().getUserId()));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return context.ack("Successfully entered the tardis");
    }

    private Response onExitTardis(SlashCommandRequest request, SlashCommandContext context) {
        try {
            if (main.getLocationManager().removeUserLocation(request.getPayload().getUserId())) {
                return context.ack("Successfully left the tardis");
            } else {
                return context.ack("You are currently not in the tardis");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    private Response onTell(SlashCommandRequest request, SlashCommandContext context) {
        try {
            Location location = main.getLocationManager().getUserLocation(request.getPayload().getUserId());
            if (location == null) {
                return context.ack("You are currently not in the tardis.");
            }
            main.getMessageManager().saveAndSendMessage(location, request.getPayload().getText());
            return context.ack("Message sent!");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Response onMeet(SlashCommandRequest request, SlashCommandContext context) {
        try {
        var location = main.getLocationManager().getUserLocation(request.getPayload().getUserId());
        if (location == null) {
            return context.ack("You are currently not in the tardis");
        }
        if (location.isAtHome()) {
            return context.ack("You are currently at home.");
        }
        var users = main.getLocationManager().find(location).stream().map(s -> "<@" + s + ">").toList();
        if (users.isEmpty()) {
            return context.ack("You are currently alone.");
        }
        return context.ack("These people are on the same timeline and location: " + String.join(", ", users));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    private Response onTravel(SlashCommandRequest request, SlashCommandContext context) {
        var place = request.getPayload().getText();
        if (place.length() > 50) {
            return context.ack("The place should not be longer than 50 characters.");
        }
        try {
            var location = main.getLocationManager().getUserLocation(request.getPayload().getUserId());
            if (location == null) {
                location = new Location(request.getPayload().getUserId());
            }
            location.setPlace(place);
            main.getLocationManager().saveLocation(location);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return context.ack("Travel completed!");
    }

    private Response onTimeTravel(SlashCommandRequest request, SlashCommandContext context) {
        int duration;
        try {
            duration = Integer.parseInt(request.getPayload().getText());
        }catch (Exception e) {
            return context.ack("This is not a number.");
        }
        try {
            var location = main.getLocationManager().getUserLocation(request.getPayload().getUserId());
            if (location == null) {
                location = new Location(request.getPayload().getUserId());
            }
            location.setCurrentDelay(duration);
            main.getLocationManager().saveLocation(location);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return context.ack("Time travel completed!");
    }

    private Response onWhereAmI(SlashCommandRequest request, SlashCommandContext context) {
        try {
            Location location = main.getLocationManager().getUserLocation(request.getPayload().getUserId());
            if (location == null) {
                return context.ack("You are currently not in the tardis");
            }
            if (location.isAtHome())
                return context.ack("You are at home");
            var output = "You are ";
            if (location.getCurrentDelay() < 0)
                output += (-location.getCurrentDelay()) + " minutes in the past ";
            else if (location.getCurrentDelay() > 0)
                output += location.getCurrentDelay() + " minutes in the future ";
            if (location.getCurrentDelay() != 0)
                output += "(" + location.getCurrentDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) + ") ";
            if (!location.getPlace().isEmpty())
                output += "in " + location.getPlace() + " ";
            return context.ack(output.trim());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
