package dev.linwood.vorkursinfo.tardisbot;

public class Configuration {
    private final String botToken;
    private final String appToken;
    private final String databaseUrl;

    public Configuration() {
        botToken = System.getenv("BOT_TOKEN");
        appToken = System.getenv("APP_TOKEN");
        databaseUrl = System.getenv("DATABASE_URL");
    }

    public String getBotToken() {
        return botToken;
    }

    public String getAppToken() {
        return appToken;
    }

    public String getDatabaseUrl() {
        return databaseUrl;
    }
}
