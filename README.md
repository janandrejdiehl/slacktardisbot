# Tardis Bot

> This bot is all about time and space

## Getting started

### Pre-requirements

* Java 17 or higher
* PostgreSQL-Database

### Environment variables

* `BOT_TOKEN` Slack bot token
* `APP_TOKEN` Slack app token
* `DATABASE_URL` PostgreSQL-JDBC URL (for example jdbc:postgresql://url:5432/databbase?user=user&password=password)

## Commands

### `/whereami`

Returns the current place and time.

### `/travel <Place>`

Travel to a place.

### `/timetravel <Minutes>`

Travel through time. If the number is negative you travel in the past.

### `/meet`

Find people in the same time and place.

### `/tell <Message...>`

Use this command to say something.

### `/entertardis`

Use this to enter the tardis without travel through time or space.

### `/exittardis`

Use this to exit the tardis and don't get new notifications.
Resets your time and space.

### `/rockpaperscissors <User> <rock/paper/scissor>`

> Escaped

Use this to play rock, paper, scissors with someone.
